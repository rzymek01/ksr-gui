/**
 * @class SimpleTasks.view.tasks.DefaultTimeWindow
 * @extends Ext.window.Window
 * 
 * A window for displaying alerts when the reminder date for a task has passed
 */
Ext.define('SimpleTasks.view.tasks.ReminderWindow', {
    extend: 'Ext.window.Window',
    xtype: 'reminderWindow',
    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox'
    ],
    width: 400,
    layout: 'anchor',
    bodyPadding: 5,

    items: [
        {
            xtype: 'box',
            cls: 'tasks-reminder-details',
            tpl: [
                '<div class="tasks-reminder-icon"></div>',
                '<p class="tasks-reminder-title">{title}</p>',
                '<p class="tasks-reminder-due">Termin: {due:date("F j, Y")}</p>'
            ]
        },
        {
            xtype: 'combobox',
            name: 'snooze_time',
            fieldLabel: 'Przypomnij mi ponownie za',
            labelWidth: 115,
            anchor: '100%',
            labelSeparator: '',
            margin: '10 0 10 20',
            forceSelection: true,
            value: 5,
            store: [
                [5, '5 minut'],
                [10, '10 minut'],
                [15, '15 minut'],
                [30, '30 minut'],
                [60, 'godzinę'],
                [120, '2 godziny'],
                [240, '4 godziny'],
                [480, '8 godzin'],
                [720, '12 godzin'],
                [1440, '1 dzień'],
                [2880, '2 dni'],
                [4320, '3 dni'],
                [5760, '4 dni'],
                [10080, 'tydzień'],
                [20160, '2 tygodnie'],
                [30240, '3 tygodnie'],
                [40320, '4 tygodnie']
            ]
        }
    ],
    buttons: [
        {
            text: 'Drzemka',
            cls: 'snooze-btn'
        },
        {
            text: 'Odrzuć',
            cls: 'dismiss-reminder-btn'
        }
    ],

    /**
     * Associates this reminder with a specific task.
     * @param {SimpleTasks.model.Task} task
     */
    setTask: function(task) {
        this.task = task;
    },
    
    /**
     * Gets the task associated with this reminder
     * @return {Task.model.Task}
     */
    getTask: function() {
        return this.task;
    }

});