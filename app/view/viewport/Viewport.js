Ext.define('SimpleTasks.view.viewport.Viewport', {
  extend: 'Ext.container.Viewport',
  xtype: 'app-main',
  controller: {
    type: 'main'
  },
  viewModel: {
    type: 'main'
  },
  itemId: 'main',
  requires: [
    'Ext.layout.container.Border'
  ],

  layout: 'border',
  session: true,

  items: [
    {
      xtype: 'tasksToolbar',
      region: 'north'
    },
    {
      xtype: 'listTree',
      region: 'west',
      width: 300,
      collapsible: true,
      split: true
    },
    {
      region: 'center',
      xtype: 'taskGrid',
      title: 'Wszystkie listy'
    },
//    {
//      xtype: 'toolbar',
//      region: 'south',
//      items: [
//        {
//          text: 'getChanges',
//          handler: 'getChangesHandler'
//        }
//      ]
//    },
  ]

});