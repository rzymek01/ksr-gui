Ext.define('SimpleTasks.view.viewport.ViewportController', {
  extend: 'Ext.app.ViewController',
  mixins: [
  ],
  requires: [
  ],
  alias: 'controller.main',
  init: function () {

  },

  getChangesHandler: function() {
    var vm = this.getViewModel(),
      session = vm.getSession(),
      changes = session.getChanges();

    console.debug('VC: changes: ', changes);
    console.debug('VC: changes: ', JSON.stringify(changes));
  }
});