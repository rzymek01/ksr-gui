/**
 * @class SimpleTasks.view.Toolbar
 * @extends Ext.toolbar.Toolbar
 */
Ext.define('SimpleTasks.view.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    xtype: 'tasksToolbar',
    items: [
        {
            text: 'Dodaj',
            iconCls: 'tasks-new',
            menu: {
                items: [
                    {
                        text: 'Nowe zadanie',
                        iconCls: 'tasks-new'
                    },
                    {
                        text: 'Nową listę',
                        iconCls: 'tasks-new-list'
                    },
//                    {
//                        text: 'Nowy folder',
//                        iconCls: 'tasks-new-folder'
//                    }
                ]
            }
        },
        {
            iconCls: 'tasks-delete-task',
            id: 'delete-task-btn',
            disabled: true,
            tooltip: 'Usuń zadanie'
        },
        {
            iconCls: 'tasks-mark-complete',
            id: 'mark-complete-btn',
            disabled: true,
            tooltip: 'Oznacz: zakończone'
        },
        {
            iconCls: 'tasks-mark-active',
            id: 'mark-active-btn',
            disabled: true,
            tooltip: 'Oznacz: aktywne'
        },
        '->',
        {
            iconCls: 'tasks-show-all',
            id: 'show-all-btn',
            tooltip: 'Wszystkie zadania',
            toggleGroup: 'status'
        },
        {
            iconCls: 'tasks-show-active',
            id: 'show-active-btn',
            tooltip: 'Aktywne zadania',
            toggleGroup: 'status'
        },
        {
            iconCls: 'tasks-show-complete',
            id: 'show-complete-btn',
            tooltip: 'Zakończone zadania',
            toggleGroup: 'status'
        }

    ]
});


