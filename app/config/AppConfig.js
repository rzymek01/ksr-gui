Ext.define('SimpleTasks.config.AppConfig', {
  alternateClassName: 'Conf',
  initialize: {
    classes: []//żadne klasy dodatkowe nie są domyślnie brane pod uwagę...
  },
  singleton: true,
  appName: 'Zadanio-pamiętywacz',
  version: 'v0.1',
  urls: {
    api: {
      regular: 'http://bottle-rzymek.rhcloud.com/do?table={entityName}',
      read: 'http://bottle-rzymek.rhcloud.com/get?table={entityName}',
      destroy: 'http://bottle-rzymek.rhcloud.com/del?table={entityName}'

//      regular: 'http://cmf.lo:10000/do?table={entityName}',
//      read: 'http://cmf.lo:10000/get?table={entityName}',
//      destroy: 'http://cmf.lo:10000/del?table={entityName}'
    }
  }
});
