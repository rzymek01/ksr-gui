Ext.define('SimpleTasks.store.Lists', {
  extend: 'Ext.data.TreeStore',
  model: 'SimpleTasks.model.List',

  sorters: 'name',
  remoteFilter: false,
  remoteSort: false,
  root: {
    expanded: true,
    id: -1,
    name: 'Wszystkie listy'
  }

});