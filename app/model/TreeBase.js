Ext.define('SimpleTasks.model.TreeBase', {
  extend: 'Ext.data.TreeModel',
  requires: [
    'cas.data.identifier.Custom',
    'Ext.data.proxy.Rest'
  ],
  idProperty: 'id',  // this is the default value (for clarity)
  clientIdProperty: 'clientId',
  identifier: 'custom', // to generate -1, -2 etc on the client
//  schema: {
//    namespace: 'SimpleTasks.model',
//    proxy: {
//      type: 'rest',
//      appendId: false,
//      actionMethods: {
////        create: 'POST', read: 'GET', update: 'PUT', destroy: 'DELETE'
//        destroy: 'POST'
//      },
//      api: {
//        destroy: SimpleTasks.config.AppConfig.urls.api.destroy
//      },
//      url: SimpleTasks.config.AppConfig.urls.api.regular,
//      pageParam: '',
//      startParam: '',
//      limitParam: '',
//      writer: {
//        allowSingle: false
////        clientIdProperty: 'clientId'
//
//      },
//      listeners: {
//        exception: function (self, request) {
//          if (request.status == 502)//bad gateway oznacza że serwer nie chodzi
//            Ext.Msg.alert('problem z połączeniem', 'nie można połączyć się z aplikacją na serwerze');
//        }
//      }
//    }
//  }
});