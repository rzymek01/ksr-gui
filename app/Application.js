Ext.define('SimpleTasks.Application', {
  extend: 'Ext.app.Application',
  name: 'SimpleTasks',
  requires: [
    'SimpleTasks.Settings',
    'SimpleTasks.config.*',
    'SimpleTasks.model.*',
    'SimpleTasks.store.*'
  ],

  controllers: [
    'Lists',
    'Tasks'
  ],
//  stores: [
//    'Lists',
//    'Tasks'
//  ],

  launch: function () {
    if (SimpleTasks.Settings.useLocalStorage && !Ext.supports.LocalStorage) {
      Ext.Msg.alert('Twoja przeglądarka nie wspiera Local Storage');
    }

    var testConfig = {
      url: 'http://bottle-rzymek.rhcloud.com/cors',
      timeout: 6000,
      method: 'get',
      disableCaching: true,
      success: this._success,
      failure: this._failure,
      scope: this
    };
    Ext.Ajax.request(testConfig);

    var testConfig2 = {
      url: 'http://bottle-rzymek.rhcloud.com/cors?table=Test&_dc=1411335715780',
      timeout: 6000,
      method: 'get',
      disableCaching: true,
      success: this._success,
      failure: this._failure,
      scope: this
    };
    Ext.Ajax.request(testConfig2);
  },

  _success: function() {
    console.info('App: cors success');
  },
  _failure: function() {
    console.error('App: cors failed');
  }
});
