Ext.define('cas.data.identifier.Custom', {
  extend: 'Ext.data.identifier.Generator',
  alias: 'data.identifier.custom',
  statics: {
    lastId: -1
  },

  generate: function () {
    return this.self.lastId--;
  }
});